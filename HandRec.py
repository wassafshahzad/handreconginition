import cv2
import numpy as np
from pynput.keyboard import Key, Controller

class HandRec:

    def __init__(self,camera):
        self.camera = camera
        self.hand_cascade = cv2.CascadeClassifier('hand.xml')
        self.keyboard = Controller()
        self.toggle = 0

    def start_video_capture(self):
        cap = cv2.VideoCapture(self.camera)
        while(True):
            _,frame = cap.read()
            hand,contour = self.detect_hand(frame)
            self.hand_detected(contour,frame,hand)
            self.press_space(contour)
            cv2.imshow("Frame",frame)
            k = cv2.waitKey(30) & 0xff
            if self.is_esc_key(k): 
                break
    
    def detect_hand(self,frame):
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) 
        hands = self.hand_cascade.detectMultiScale(gray, 1.5, 2) 
        contour = hands 
        contour = np.array(contour)
        return hands,contour

    def press_space(self,contour):
        if(self.toggle==1 and len(contour)<=0):
            self.keyboard.press(Key.space)
            
    def hand_detected(self,contour,frame,hand):
        if(len(contour)>0):
                self.toggle = 1
                self.draw_rect(hand,frame)

    def is_esc_key(self,k):
        return k == 27

    def draw_rect(self,hands,frame):
        for (x, y, w, h) in hands: 
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2) 

hand = HandRec(0)
hand.start_video_capture()
